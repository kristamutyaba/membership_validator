package membershipvalidator;

/**
 * Validation source code.
 */
import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;

public class MembershipValidator extends JApplet {
    private JTextField jtfMembershipId = new JTextField(6);
    private JButton jbtVarify = new JButton("Varify");
    private JButton jbtExit = new JButton("Exit");

    /**
     * Prepared statement for executing queries
     */
    private PreparedStatement preparedStatement;

    /**
     * Initialize the applet
     */
    public void init() {
        /** Initialize database connection and create a Statement object */
        initializeDB();

        jbtVarify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {

                jbtVarify_actionPerformed(e);
            }
        });


        jbtExit.addActionListener(new ActionListener() {
                                      public void actionPerformed(ActionEvent e) {
                                          System.exit(0);
                                      }
                                  });

        /** Membership id panel */
        JPanel jPanel = new JPanel();
        jPanel.add(new JLabel("Enter Membership Id "));
        jPanel.add(jtfMembershipId);
        jPanel.add(jbtVarify);
        jPanel.add(jbtExit);

        add(jPanel, BorderLayout.NORTH);
    }

    private void initializeDB() {
        try {
            /** Load JDBC driver */
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver Load");

            /** Establish Connection to the database */
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/membershipValidator", "root", "");
            System.out.println("Connection Established");

            /** Place holder */
            String queryString = "select membershipId, phoneNumber, membershipState, " +
                    "memberSurname, memberLastName from Member where Member.membershipId=?";

            /** Create a statement */
            preparedStatement = connection.prepareStatement(queryString);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbtVarify_actionPerformed(ActionEvent e){

        /** Set getText() method to jtfMembershipId */
        String membershipId = jtfMembershipId.getText();
        try{
            preparedStatement.setString(1, membershipId);
            ResultSet rset = preparedStatement.executeQuery();

            if(rset.next()) {

                String phoneNumber = rset.getString(2);
                String membershipState = rset.getString(3);
                String memberSurname = rset.getString(4);
                String memberLastName = rset.getString(5);



                /** Display result in a dialog box */
                JOptionPane.showMessageDialog(null,"VALID IDENTIFICATION\n" +
                        "\nId number: "+ membershipId + "\nPhone Number: " +
                        phoneNumber + "\nMembership State: " + membershipState + "\nFirst Name: "
                        + memberSurname +
                         "\nLast Name: " + memberLastName
                        );
            }
            else {
                /** Display result in a dialog box */
                JOptionPane.showMessageDialog(null, "Invalid Id Number");
            }
            }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    }


